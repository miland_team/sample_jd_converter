# Sample JD Converter #

This sample project implements the conversion between Julian Day numbers (JD) and Calendar Dates (DT) and vice versa, allowing the usage negative years and negative JDs as well.

In the conversion from JD to DT the date format is returned following Meuus’ suggestion, that is a double such as YYYY.MMDDdd.
As for the conversion JD to DT, with negative JD, because of rounding, it may happen that there might be little differences in the decimal part, for instance, taking the JD from a date YYYY, M, DDxx, and converting it back to DT, the result might be YYYY.MMDD(xx-1), mainly because YYYY.MMDDxx = YYYY.MMDD(xx-1)99999, but rounding to 2 decimal places xx.9999 is returned as xx-1 and not xx.

I hope I have been clear enough to explain this point.

Working with negative JD, if the precision required is not very high, say 1 or 2 decimal places, the results are quite satisfactory.
Saying that JD can be negative means that the correspondent date is before 4713-01-01 BC (-4712-01-01).
Important to keep in mind is that, according to convention, 1 BC is 0, 2 BC is −1, and so on, hence 4713 BC is −4712.

The algorithm used is the one proposed by Jean Meuus in his book Astronomical Formulae for Calculators (1985), but it has been revisited to correctly manage rounding due to JD negative numbers.

The programming language used is C#, but it shouldn't be hard to convert it into other languages such as C, C++ etc.

The algorithm used is not intended to both reach high precision and performance targets. After several tests it seems to give correct results both converting dates to JD and JD to dates.

The reason for this algorithm is mainly due to the fact that I am currently interested in ancient astronomy and it often happens to come across dates that are far before 4713 BC and even in this context the JD numbers are useful for performing astronomical computations.

I decided to share this code in the hope that it can be useful to someone else should he/she need to perform calculations with ancient dates or negative JD numbers.
Despite there is an algorithm that can manage negative JD and return dates, I couldn't manage to find information about that in the internet,
that is why, after lost my last hope, I decided to implement it myself, starting from Meuus' algorithm, performing several tests and finding the correct way to round numbers, specifically using negative JD,
I hope I manged to reach this target.

The code has been released according to the license policy reported below.

Thank you and good bytes.

>(C) 2017 MilAnd S.H.

> This project can be distributed under the Boost Software License.