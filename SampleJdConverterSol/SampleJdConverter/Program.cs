﻿using System;

namespace SampleJdConverter
{
    class Program
    {
        static void Main(string[] args)
        {

            string cont = "y";
            while (cont.Equals("y"))
            {
                Console.Write("Year: ");
                var yr = Console.ReadLine();
                int y = int.Parse(yr);
                Console.WriteLine();

                Console.Write("Month: ");
                var mt = Console.ReadLine();
                int m = int.Parse(mt);
                Console.WriteLine();

                Console.Write("Day: ");
                var day = Console.ReadLine();
                double d = double.Parse(day);
                Console.WriteLine();

                double jd = JdHelper.GetJdFromDate(y, m, d);
                Console.WriteLine("JD: {0:n2}", jd);

                var date = JdHelper.GetDateFromJd(jd);
                Console.WriteLine("Date: {0:n6}", date);

                Console.Write("JD: ");
                var strJd = Console.ReadLine();
                jd = double.Parse(strJd);
                Console.WriteLine();
                date = JdHelper.GetDateFromJd(jd);
                Console.WriteLine("Date: {0:n6}", date);

                Console.WriteLine("Continue? (y/n)");
                cont = Console.ReadLine();

            }
        }
    }
}