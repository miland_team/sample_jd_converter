﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleJdConverter
{
    public class JdHelper
    {
        /// <summary>
        /// Returns the Julian Day number for the given date.
        /// <para>
        /// The date can contain a negative year.
        /// The result can be a negative JD when the date is set before -4712-01-01 (= 4713-01-01 b.C.)
        /// </para>
        /// <para>
        /// Floor: returns the largest integer less than or equal to the specified double-precision floating-point number (cf. MSDN), e.g. 15.826 = 15; -15.826 = -16.
        /// </para>
        /// <para>
        /// Truncate: returns the integral part of a number (cf. MSDN), e.g. 15.826 = 15; -15.826 = -15.
        /// </para>
        /// </summary>
        /// <param name="TheYear"></param>
        /// <param name="TheMonth"></param>
        /// <param name="TheDay"></param>
        /// <returns></returns>
        public static double GetJdFromDate(int TheYear, int TheMonth, double TheDay)
        {
            var yyyy = (double)TheYear;
            var mm = (double)TheMonth;

            if (mm < 3)
            {
                --yyyy;
                mm += 12;
            }

            double a = 0;
            double b = 0;

            // Build the date string as YYYY.MMDDdd
            var strDate = $"{yyyy.ToString()}.{ ((int)mm).ToString("D2")}{((int)(TheDay * 1e2)).ToString("D4")}";
            // Convert to double
            var dblDate = Convert.ToDouble(strDate);

            if (dblDate >= 1582.1015)
            {
                a = (int)(yyyy / 100);
                b = 2 - a + (int)(a / 4);
            }

            // The decimal part of the day must be taken with same sign of the JD number.
            // This in order to manage negative JD numbers => years < -4712
            double fracDay = TheDay - Math.Truncate(TheDay);
            fracDay = (yyyy < -4712 ? -1 : 1) * fracDay;
            var jd = Math.Floor(365.25 * (yyyy + 4716)) + Math.Floor(30.6 * (mm + 1)) + (int)TheDay + fracDay + b - 1524.5;

            // When JD is negative adjustment for rounding
            jd = jd < 0 ? ++jd : jd;

            return jd;
        }

        /// <summary>
        /// Returns the date in the format YYYY.MM.DDdd for the given JD number.
        /// <para>
        /// The JD number can be negative (=> date &lt; -4712-01-01).
        /// </para>
        /// <para>
        /// Floor: returns the largest integer less than or equal to the specified double-precision floating-point number (cf. MSDN), e.g. 15.826 = 15; -15.826 = -16.
        /// </para>
        /// <para>
        /// Truncate: returns the integral part of a number (cf. MSDN), e.g. 15.826 = 15; -15.826 = -15.
        /// </para>
        /// </summary>
        /// <param name="Jd"></param>
        /// <returns></returns>
        public static double GetDateFromJd(double Jd)
        {
            double compute = 0.0d;
            Jd += .5;
            // When JD is negative adjustment for rounding
            Jd = Jd < 0 ? --Jd : Jd;
            int intJd = (int)Jd;
            // Compute the absolute value of the fractional part of JD 
            double frcJd = Math.Abs(Jd - Math.Truncate(Jd));
            int A = 0;

            // 2299161 JD = 1582.1015 date
            if (intJd < 2299161)
                A = intJd;
            else
            {
                compute = (intJd - 1867216.25) / 36524.25;
                int alpha = (int)Math.Floor(compute);
                compute = alpha / 4.0d;
                A = intJd + 1 + alpha - (int)Math.Floor(compute);
            }

            int B = A + 1524;
            compute = (B - 122.1) / 365.25;
            int C = (int)Math.Floor(compute);
            compute = 365.25 * C;
            int D = (int)Math.Floor(compute);
            compute = (B - D) / 30.6001;
            int E = (int)Math.Floor(compute);

            compute = 30.6001 * E;
            double DDdd = B - D - Math.Floor(compute) + frcJd;
            int month = E < 14 ? E - 1 : E - 13;
            int year = month < 3 ? C - 4715 : C - 4716;

            // Build the date string as YYYY.MMDDdd - the day is rounded to 2 decimal places
            var strDate = $"{year.ToString()}.{month.ToString("D2")}{((int)(Math.Round(DDdd * 1e2, 2))).ToString("D4")}";
            // Convert to double
            var dblDate = Convert.ToDouble(strDate);

            return dblDate;
        }
    }
}
